package com.harcourt.hoap.web.assignment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hrw.error.HRWTimeoutException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import util.StringUtil;

import com.harcourt.activity.ActivityManager;
import com.harcourt.activity.util.ResourceTypeUtility.IResourceType;
import com.harcourt.activity.vo.ActivityValue;
import com.harcourt.activity.vo.TOCItemActivity;
import com.harcourt.assignment.AssignmentManager;
import com.harcourt.assignment.vo.AssignmentInfoValue;
import com.harcourt.assignment.vo.AssignmentValue;
import com.harcourt.hoap.util.ApplicationKeys;
import com.harcourt.organization.classroom.ClassroomManager;
import com.harcourt.organization.classroom.ClassroomProductManager;
import com.harcourt.preferences.CalendarPreferencesManager;
import com.harcourt.preferences.GeneralPreferencesManager;
import com.harcourt.preferences.InterventionPreferencesManager;
import com.harcourt.preferences.TestPreferencesManager;
import com.harcourt.preferences.vo.CalendarPreferences;
import com.harcourt.preferences.vo.GeneralPreferences;
import com.harcourt.preferences.vo.InterventionPreferences;
import com.harcourt.preferences.vo.TestPreferences;
import com.harcourt.session.ISessionManager;
import com.harcourt.user.vo.UserValue;
import com.harcourt.util.ESAPIStringValidator;
import com.hmhco.api.jaxb.sif3.Persons;
import com.hrw.adaptiveservice.AdaptiveServiceHandler;
import com.hrw.adaptiveservice.data.AdaptiveServiceData;


/**
 * The ReportsController will be responsible for rendering the reports menu page of the HOAP application.
 * 
 * @author bclemenzi
 */
public class Test implements Controller 
{
    /** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());
    
    /** */
    private ISessionManager m_sessionManager;
    
    /** */
    private ClassroomManager m_classroomManager;
    
    /** */
    private ActivityManager m_activityManager;
    
    /** */
    private AssignmentManager m_assignmentManager;

    /**
     * The handleRequest method will generate the reports menu view for the end user.  The display 
     * will be rendered by utilizing a jsp utilized by Spring's ModelAndView object.
     * 
     * @param HttpServletRequest
     * @param HttpServletResponse
     */
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	ModelAndView modelView;
		Map modelMap = new HashMap();
		
		// Get a handle to the the user session object, then retrieve the user value object.
		m_sessionManager = (ISessionManager)request.getAttribute(ApplicationKeys.Session.SESSION_MANAGER);
		
        // Invoke the action method requested.
        String action = "display";
		if (request.getParameter(ApplicationKeys.HttpRequest.ACTION) != null)
		{
		    action = request.getParameter(ApplicationKeys.HttpRequest.ACTION);
		}
		
		if(action.equalsIgnoreCase("process"))
		{
		    modelView = process(request, modelMap);
		}
		else
		{
		    modelView = display(request, modelMap, null);
		}
        
        return modelView;
    }
    
    /**
     * 
     * @param request
     * @param modelMap
     * @param errorMessage
     * @return
     */
    private ModelAndView display(HttpServletRequest request, Map modelMap, String systemMessage)
    {
    	// Get session variables
		UserValue userValue = (UserValue)m_sessionManager.getValue(ApplicationKeys.Session.USER_VALUE);
    	String classroomId = (String)m_sessionManager.getValue(ApplicationKeys.Session.CLASSROOM_ID);
    	String isbn = (String)m_sessionManager.getValue(ApplicationKeys.Session.ISBN);
    	String contentId = StringUtil.emptyIfNull((String)request.getParameter("contentId"));
    	boolean isAlchemy = (StringUtil.emptyIfNull(request.getParameter("isAlchemy")).equals("true")) ? true : false;
    	
    	modelMap.put("returnUrl", StringUtil.emptyIfNull((String)request.getParameter("returnUrl")));
    	modelMap.put("isChildWindow", StringUtil.emptyIfNull((String)request.getParameter("isChildWindow")));
    	
        if(!StringUtil.isNullOrEmpty(systemMessage))
        {
            modelMap.put("systemMessage", systemMessage);
        }
        
    	try
		{
			Collection assignedStudents = new ArrayList();
			AssignmentValue assignmentValue = new AssignmentValue();
			AssignmentInfoValue assignmentInfoValue = new AssignmentInfoValue();
			
			Collection students = m_classroomManager.getStudentsInClass(classroomId);
			
			
			
			//Added for FLIT-476
			Collection classrooms = m_classroomManager.getActiveClassroomsByUserId(userValue.getUserId());
			if(!StringUtil.isNullOrEmpty(classroomId))
    		{
    			ClassroomProductManager classroomProductManager = ClassroomProductManager.getClassroomProducts(classroomId);
    			Collection secureProgram = classroomProductManager.getSecureProgramSubscriptions();
    			
    			modelMap.put("books", secureProgram);
    		}
			modelMap.put("classrooms", classrooms);
			// Added for Non-Assessment Enhancement
			
     		modelMap.put("isbn", isbn);
    		modelMap.put("classroomId", classroomId);
    		modelMap.put("students", students);
    		modelMap.put("assignmentId", "");
    		modelMap.put("assignmentValue", assignmentValue);
			modelMap.put("assignmentInfoValue", assignmentInfoValue);
			modelMap.put("assignedStudents", assignedStudents);
			modelMap.put("studentActivityValues",new ArrayList());
			modelMap.put("studentAssignmentValues", new ArrayList());
			modelMap.put("studentActivityDataMap", new HashMap());
			modelMap.put("userValue", userValue);
		   	modelMap.put("actionUrl", "createassignment.htm");
			
			if (isAlchemy)
			{
				ActivityValue activity = m_activityManager.getActivity(contentId);
				modelMap.put("activityValue", activity);
			}
			else
			{
				TOCItemActivity activity = AssignmentManager.getTOCItemActivity(contentId);
				modelMap.put("activityValue", activity);
			}			
			
			// this form displays differently for settings vs. assign again, so we need to differentiate
			modelMap.put("currentView","assignagain");
			
			// Add the test preferences object.
			TestPreferencesManager testPreferencesManager = new TestPreferencesManager();
			TestPreferences testPreferences = testPreferencesManager.getTestPreferences(userValue.getUserId());
			modelMap.put("testPreferences", testPreferences);			
			
			// Add the general preferences object.
			GeneralPreferencesManager generalPreferencesManager = new GeneralPreferencesManager();
			GeneralPreferences generalPreferences = generalPreferencesManager.getGeneralPreferences(userValue.getUserId());
			modelMap.put("generalPreferences", generalPreferences);
			
			// Add the calendar preferences object.
			String preferenceKey = userValue.getUserId() + "-" + classroomId;
			CalendarPreferencesManager calendarPreferencesManager = new CalendarPreferencesManager();
			CalendarPreferences calendarPreferences = calendarPreferencesManager.getCalendarPreferences(preferenceKey);
			modelMap.put("calendarPreferences", calendarPreferences);
			
			CalendarPreferences holidayPreferences = calendarPreferencesManager.getCalendarPreferences(userValue.getUserId());
			modelMap.put("holidayPreferences", holidayPreferences);
			
			// Add the intervention preferences object.
			InterventionPreferencesManager interventionPreferencesManager = new InterventionPreferencesManager();
			InterventionPreferences interventionPreferences = interventionPreferencesManager.getInterventionPreferences(userValue.getUserId()+ "-" + classroomId + "-" + isbn);
			modelMap.put("interventionPreferences", interventionPreferences);
		}
		catch (Exception e)
		{
            logger.error(e.getMessage());
            modelMap.put("systemMessage", "Unable to load assignment information");
		}
		
    	return new ModelAndView("assignment/assignment-settings-form.jsp", modelMap);
    }
    /**
     * 
     * @param request
     * @param modelMap
     * @return
     */
    private ModelAndView process(HttpServletRequest request, Map modelMap)
    {   	
    	modelMap.put("returnUrl", StringUtil.emptyIfNull((String)request.getParameter("returnUrl")));
    	modelMap.put("isChildWindow", StringUtil.emptyIfNull((String)request.getParameter("isChildWindow")));
    	String assgnmntNotes = StringUtil.emptyIfNull(request.getParameter("notes"));
    	
    	
    	String errorMessage = ESAPIStringValidator.checkAssgmntNotes(assgnmntNotes, true);
    	
		String classroomId = (String)m_sessionManager.getValue(ApplicationKeys.Session.CLASSROOM_ID);
		UserValue user = (UserValue)m_sessionManager.getValue(ApplicationKeys.Session.USER_VALUE); 
		String isbn = (String)m_sessionManager.getValue(ApplicationKeys.Session.ISBN);
		String resType = null; //Added for FLIT-476 to get the resource type of the activity
		AdaptiveServiceHandler serviceHandler = null;	
		//Added for CMIPR-545 to retrieve the activity name from the JSP
		String activityName =  StringUtil.emptyIfNull((String)request.getParameter("activityName"));
		
		//cmipr-2179
		String assignPrefPercentage = StringUtil.emptyIfNull(request.getParameter("selectAssignPercent"));
    	try
		{
        	String studentList[] = request.getParameterValues("students");
        	ArrayList studentIds = new ArrayList(Arrays.asList(studentList));
			
        	// Populate the assignment and assignment info objects from the http request.
			AssignmentValue assignment = AssignmentManager.populateAssignmentValueFromHttpRequest(request, classroomId, user.getUserId(), isbn);
			String resourceType = StringUtil.emptyIfNull(request.getParameter("resType"));
			assignment.setResourceCategory(resourceType);
			AssignmentInfoValue assignmentInfo = AssignmentManager.populateAssignmentInfoValueFromHttpRequest(request, assignment);
			//Added for CMIPR-545 to set the activity name associated with the assignment
			assignmentInfo.setActivityName(activityName);
			//cmipr-2179
			assignmentInfo.setAssignPrefPercentage(assignPrefPercentage);
			
			// Create the assignment.
			assignment.setAssignmentInfo(assignmentInfo);			
			
						
			// Populate the content id for the confirm page.
			String contentId = null;
			
			if (assignmentInfo.getTOCItemActivityId() != null)
			{
				modelMap.put("isAlchemy", "false");
				contentId = assignmentInfo.getTOCItemActivityId();
			}
			else
			{
				modelMap.put("isAlchemy", "true");
				contentId = assignmentInfo.getContentObjectId();
			}	
			
			//For calling the Adaptive Service for Knewton Product registration for a student: CMIPR-137
			AdaptiveServiceHandler adaptiveServiceHandler = new AdaptiveServiceHandler();
			boolean isKnewtonUser = adaptiveServiceHandler.isKnewtonUser(isbn,classroomId);
			AdaptiveServiceData adaptiveServiceData = null;
			Map studentRegResponse = null;
			Map goalIds = null; 
			if(isKnewtonUser) {
				//Modified the below method to handle both student registration and goal creation for CMIPR-545
				adaptiveServiceData = adaptiveServiceHandler.createStudentRegAndCreateGoal(user.getUserId(), studentIds, isbn, classroomId, assignment);
				studentRegResponse = adaptiveServiceData.getStudentRegResponse();
				goalIds = adaptiveServiceData.getGoalIds();
			}
			
			if(!StringUtil.isNullOrEmpty(errorMessage)){    		
		    	modelMap.put("systemMessage", errorMessage);		    	
		    	}
			else{
			m_assignmentManager.createAssignment(assignment, studentIds);
			}
			String assignmentId = assignment.getAssignmentID();
			String learningInstanceId = "";
			Persons respPersons = null;
			String assignmentType = "";
			if (assignment.getAssignmentInfo().getResourceType() == IResourceType.WARM_ASSIGNMENT_ENRICHMENT) {
				assignmentType = "W";
			}
			if (studentRegResponse != null) {
				respPersons = (Persons) studentRegResponse.get("respPersons");
				learningInstanceId = studentRegResponse.get("LIID").toString();
			}
			
			//Save the student registration details and learning goal details on the HMOF DB
			if (respPersons != null || goalIds != null) {
				try {
					adaptiveServiceHandler.saveStudentRegistrationAndGoal(respPersons, learningInstanceId,
						goalIds, assignmentId);					
				}
				catch (Exception e)
				{
		            logger.error(e);
		            m_assignmentManager.deleteAssignment(assignmentId);
		            return display(request, modelMap, "An error occurred while creating the assignment. Please try again later.");
				}
				
			}
			
			modelMap.put("contentId", contentId);
			
			resType = Integer.toString(assignmentInfo.getResourceType());			
			modelMap.put("resType", resType);
			
			if(!StringUtil.isNullOrEmpty(errorMessage)){ 
				return display(request, modelMap, errorMessage);		    	
		    	}			
		}
		catch (HRWTimeoutException e)
		{
            logger.error("Assignment creation timed out", e);
            return display(request, modelMap, "The assignment creation has timed out. Please try again later.");
		}
		catch (Exception e)
		{
            logger.error(e);
            return display(request, modelMap, "An error occurred while creating the assignment. Please try again later.");
		}

    	return new ModelAndView("assignment/assignment-settings-confirm.jsp", modelMap);
    }
    
	/**
	 * 
	 * @param classroomManager
	 * 
	 * @author Brendan Clemenzi
	 */
    public void setClassroomManager(ClassroomManager classroomManager) 
    {
    	m_classroomManager = classroomManager;
    }

    /**
     * 
     * @return
     * 
     * @author Brendan Clemenzi
     */
    public ClassroomManager getClassroomManager() 
    {
        return m_classroomManager;
    }
    
	/**
	 * 
	 * @param activityManager
	 * 
	 * @author Brendan Clemenzi
	 */
    public void setActivityManager(ActivityManager activityManager) 
    {
    	m_activityManager = activityManager;
    }

    /**
     * 
     * @return m_activityManager
     * 
     * @author Brendan Clemenzi
     */
    public ActivityManager getActivityManager() 
    {
        return m_activityManager;
    }
    
	/**
	 * 
	 * @param assignmentManager
	 * 
	 * @author Brendan Clemenzi
	 */
    public void setAssignmentManager(AssignmentManager assignmentManager) 
    {
    	m_assignmentManager = assignmentManager;
    }

    /**
     * 
     * @return m_assignmentManager
     * 
     * @author Brendan Clemenzi
     */
    public AssignmentManager getAssignmentManager() 
    {
        return m_assignmentManager;
    }
}
