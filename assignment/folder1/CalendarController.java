package com.harcourt.hoap.web.calendar;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lycea.util.LyceaDate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import util.StringUtil;

import com.harcourt.assignment.AssignmentManager;
import com.harcourt.assignment.vo.AssignmentInfoValue;
import com.harcourt.assignment.vo.AssignmentValue;
import com.harcourt.hoap.util.ApplicationKeys;
import com.harcourt.organization.classroom.ClassroomManager;
import com.harcourt.organization.classroom.vo.ClassroomValue;
import com.harcourt.preferences.CalendarPreferencesManager;
import com.harcourt.preferences.vo.CalendarPreferences;
import com.harcourt.session.ISessionManager;
import com.harcourt.user.vo.UserValue;
import com.harcourt.util.CalendarUtility;
//Added for CSI-179
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration; 
/**
 * The SplashController will be responsible for rendering the welcome page of the HOAP application.
 * 
 * @author bclemenzi
 */
public class CalendarController implements Controller 
{
    /** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    /** */
    private ClassroomManager m_classroomManager;
    
    /** */
    private AssignmentManager m_assignmentManager;
    
    /** */
    private CalendarPreferencesManager m_preferencesManager;
    
    /** */
    private ISessionManager m_sessionManager;

    /**
     * The handleRequest method will generate the welcome view for the end user.  The display 
     * will be rendered by utilizing a jsp utilized by Spring's ModelAndView object.
     * 
     * @param HttpServletRequest
     * @param HttpServletResponse
     */
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	ModelAndView modelView;
		Map modelMap = new HashMap();
		
		// Get a handle to the the user session object, then retrieve the user value object.
		m_sessionManager = (ISessionManager)request.getAttribute(ApplicationKeys.Session.SESSION_MANAGER);
		
        // Invoke the action method requested.
        String action = "display";
		if (request.getParameter(ApplicationKeys.HttpRequest.ACTION) != null)
		{
		    action = request.getParameter(ApplicationKeys.HttpRequest.ACTION);
		}
		
		// Determine what action to invoke.
		if(action.equalsIgnoreCase("process"))
		{
		    modelView = process(request, modelMap);
		}
		else if(action.equalsIgnoreCase("noninstructional"))
		{
		    modelView = nonInstructional(request, modelMap);
		}
		else if(action.equalsIgnoreCase("addnoninstructional"))
		{
		    modelView = addNonInstructional(request, modelMap);
		}
		else
		{
		    modelView = display(request, modelMap, null);
		}
        
        return modelView;
    }
    
    /**
     * 
     * @param request
     * @param modelMap
     * @param errorMessage
     * @return
     */
    private ModelAndView display(HttpServletRequest request, Map modelMap, String errorMessage)
    {
		UserValue userValue = (UserValue)m_sessionManager.getValue(ApplicationKeys.Session.USER_VALUE);
    	String classroomId = (String)m_sessionManager.getValue(ApplicationKeys.Session.CLASSROOM_ID);
    	String isbn = (String)m_sessionManager.getValue(ApplicationKeys.Session.ISBN);
    	String selectedDate = StringUtil.emptyIfNull(request.getParameter("selectedDate"));
    	
        if(!StringUtil.isNullOrEmpty(errorMessage))
        {
            modelMap.put("errorMessage", errorMessage);
        }
    	
    	try
		{  		
    		// Get today's assignments
    		LyceaDate todaysDate = new LyceaDate();   	
    		if(!StringUtil.isNullOrEmpty(selectedDate))
    		{
    			todaysDate = CalendarUtility.convertToLyceaDate(selectedDate);
    		}
    		
    		LyceaDate firstOfTheMonth = CalendarUtility.getFirstOfTheMonth(todaysDate.toString());
    		LyceaDate lastOfTheMonth = CalendarUtility.getlastOfTheMonth(todaysDate.toString());
    		
    		Collection assignments;
    		if(StringUtil.isNullOrEmpty(isbn))
    		{
    			assignments = m_assignmentManager.getAssignmentsByClassroom(userValue.getUserId(), classroomId, firstOfTheMonth, lastOfTheMonth);
    		}
    		else
    		{
    			assignments = m_assignmentManager.getAssignmentsByClassroom(userValue.getUserId(), classroomId, isbn, firstOfTheMonth, lastOfTheMonth);
    		}
    		
			for(Iterator iterator = assignments.iterator(); iterator.hasNext();)
			{
				AssignmentValue assignment = (AssignmentValue)iterator.next();
				AssignmentInfoValue assignmentInfoVal = assignment.getAssignmentInfo();
				// Modified getAssignmentInfoNoLycea() method with an additional parameter for No. of students started the assignment
				// Since this functionality doesn't involve the logic to get the No. of students started the Assignment, passing '0'
				// as numStarted.
//				assignment.setAssignmentInfo(m_assignmentManager.getAssignmentInfoNoLycea(assignment.getAssignmentID(),"HOAP.ACTIVITY.GRADE",0));
				
				// Modified for FWK-776. For Non-Assessments this method should not be called which will override the 
				// non-assessment assignment info.
				if(assignmentInfoVal.getResourceType() != 11){
					assignment.setAssignmentInfo(m_assignmentManager.getAssignmentInfoNoLycea(assignment.getAssignmentID(),"HOAP.ACTIVITY.GRADE"));
				}
			}

			String preferenceKey = userValue.getUserId() + "-" + classroomId;
    		CalendarPreferences calendarPreferences = m_preferencesManager.getCalendarPreferences(preferenceKey);
    		
    		CalendarPreferences holidayPreferences = m_preferencesManager.getCalendarPreferences(userValue.getUserId());
			
			// Add the required objects to the model map, for use within the jsp.
    		modelMap.put("selectedDate", todaysDate.toString());
    		modelMap.put("calendarPreferences", calendarPreferences);
    		modelMap.put("holidayPreferences", holidayPreferences);
			modelMap.put("assignments", assignments);
			modelMap.put("userValue", userValue);
			modelMap.put("classroomId", classroomId);
			modelMap.put("isbn", isbn);
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
		}
		
		return new ModelAndView("calendar/calendar.jsp", modelMap);
    }
    
    /**
     * 
     * @param request
     * @param modelMap
     * @return
     */
    private ModelAndView process(HttpServletRequest request, Map modelMap)
    {
		return new ModelAndView("calendar/calendar.jsp", modelMap);
    }
    
    /**
     * The process method is responsible for rendering the centent menu with classroom and book data.
     * 
     * @param request
     * @param modelMap
     * @return
     */
    private ModelAndView nonInstructional(HttpServletRequest request, Map modelMap)
    {
    	UserValue userValue = (UserValue)m_sessionManager.getValue(ApplicationKeys.Session.USER_VALUE);
    	String classroomId = (String)m_sessionManager.getValue(ApplicationKeys.Session.CLASSROOM_ID);
    	
    	String sunday = StringUtil.emptyIfNull(request.getParameter("sunday"));
    	String monday = StringUtil.emptyIfNull(request.getParameter("monday")); 
    	String tuesday = StringUtil.emptyIfNull(request.getParameter("tuesday"));
    	String wednesday = StringUtil.emptyIfNull(request.getParameter("wednesday"));
    	String thursday = StringUtil.emptyIfNull(request.getParameter("thursday"));
    	String friday = StringUtil.emptyIfNull(request.getParameter("friday"));
    	String saturday = StringUtil.emptyIfNull(request.getParameter("saturday"));
    	String applyToAll = StringUtil.emptyIfNull(request.getParameter("applyToAll"));
    	
    	try
    	{
    		if(applyToAll.equalsIgnoreCase("true"))
    		{
    			Collection classrooms = m_classroomManager.getActiveClassroomsByUserId(userValue.getUserId());
    			
				for (Iterator i = classrooms.iterator(); i.hasNext();)
				{
					ClassroomValue classroomValue = (ClassroomValue)i.next();
					
		    		String preferenceKey = userValue.getUserId() + "-" + classroomValue.getClassroomId();
		    		CalendarPreferences calendarPreferences = m_preferencesManager.getCalendarPreferences(preferenceKey);
		    		
		    		calendarPreferences.setSunday(Boolean.valueOf(sunday).booleanValue());
		    		calendarPreferences.setMonday(Boolean.valueOf(monday).booleanValue());
		    		calendarPreferences.setTuesday(Boolean.valueOf(tuesday).booleanValue());
		    		calendarPreferences.setWednesday(Boolean.valueOf(wednesday).booleanValue());
		    		calendarPreferences.setThursday(Boolean.valueOf(thursday).booleanValue());
		    		calendarPreferences.setFriday(Boolean.valueOf(friday).booleanValue());
		    		calendarPreferences.setSaturday(Boolean.valueOf(saturday).booleanValue());
		
		    		m_preferencesManager.setWeekdayPreferences(preferenceKey, calendarPreferences);
				}
    		}
    		else
    		{
	    		String preferenceKey = userValue.getUserId() + "-" + classroomId;
	    		CalendarPreferences calendarPreferences = m_preferencesManager.getCalendarPreferences(preferenceKey);
	    		
	    		calendarPreferences.setSunday(Boolean.valueOf(sunday).booleanValue());
	    		calendarPreferences.setMonday(Boolean.valueOf(monday).booleanValue());
	    		calendarPreferences.setTuesday(Boolean.valueOf(tuesday).booleanValue());
	    		calendarPreferences.setWednesday(Boolean.valueOf(wednesday).booleanValue());
	    		calendarPreferences.setThursday(Boolean.valueOf(thursday).booleanValue());
	    		calendarPreferences.setFriday(Boolean.valueOf(friday).booleanValue());
	    		calendarPreferences.setSaturday(Boolean.valueOf(saturday).booleanValue());
	
	    		m_preferencesManager.setWeekdayPreferences(preferenceKey, calendarPreferences);
    		}
    	}
    	catch(Exception e)
    	{
    		logger.error(e.getMessage());
    		return display(request, modelMap, e.getMessage());
    	}

		return display(request, modelMap, null);
    }
    
    /**
     * The process method is responsible for rendering the centent menu with classroom and book data.
     * 
     * @param request
     * @param modelMap
     * @return
     */
    private ModelAndView addNonInstructional(HttpServletRequest request, Map modelMap)
    {
    	UserValue userValue = (UserValue)m_sessionManager.getValue(ApplicationKeys.Session.USER_VALUE);
    	String classroomId = (String)m_sessionManager.getValue(ApplicationKeys.Session.CLASSROOM_ID);
    	
    	String selectedDateString = StringUtil.emptyIfNull(request.getParameter("selectedDate"));
    	String durationString = StringUtil.emptyIfNull(request.getParameter("duration")); 
    	String applyToAll = StringUtil.emptyIfNull(request.getParameter("applyToAll"));
    	String removeSelectedDay = StringUtil.emptyIfNull(request.getParameter("removeSelectedDay"));
    	
    	try
    	{
    		if(removeSelectedDay.equalsIgnoreCase("true"))
    		{
    			//CSI-179-Changes Start
    			String preferenceKey =null;
    			int duration = Integer.parseInt(durationString);
				String selectedDateArray[] =new String[duration];
				selectedDateArray[0]=selectedDateString;
				//Get All the dates in selectedDateArray
				populateNonInstructionalDays(selectedDateArray);
    			//CSI-179-Changes End
	    		if(applyToAll.equalsIgnoreCase("true"))
				{
					Collection classrooms = m_classroomManager.getActiveClassroomsByUserId(userValue.getUserId());

					for (Iterator i = classrooms.iterator(); i.hasNext();)
					{
						ClassroomValue classroomValue = (ClassroomValue)i.next();
						preferenceKey = userValue.getUserId() + "-" + classroomValue.getClassroomId();
						if(duration > 1){ 
							for(int j = 0; j < selectedDateArray.length; j++)
								m_preferencesManager.removeNonInstructionalDay(preferenceKey, selectedDateArray[j]);
						}else{
							m_preferencesManager.removeNonInstructionalDay(preferenceKey, selectedDateString);
						}
						//m_preferencesManager.removeNonInstructionalDay(preferenceKey, selectedDateString);
					}
				}
				else
				{
					preferenceKey = userValue.getUserId() + "-" + classroomId;
					//m_preferencesManager.removeNonInstructionalDay(preferenceKey, selectedDateString);
					if(duration > 1){
						for(int i = 0; i < selectedDateArray.length; i++)
							m_preferencesManager.removeNonInstructionalDay(preferenceKey, selectedDateArray[i]);
					}else{
						m_preferencesManager.removeNonInstructionalDay(preferenceKey, selectedDateString);
					}
				}
	    		//CSI-179-Changes End
    		}
    		else
    		{    		
	    		if(applyToAll.equalsIgnoreCase("true"))
	    		{
	    			Collection classrooms = m_classroomManager.getActiveClassroomsByUserId(userValue.getUserId());
	    			
					for (Iterator i = classrooms.iterator(); i.hasNext();)
					{
						ClassroomValue classroomValue = (ClassroomValue)i.next();
						
			    		String preferenceKey = userValue.getUserId() + "-" + classroomValue.getClassroomId();
			    		CalendarPreferences calendarPreferences = m_preferencesManager.getCalendarPreferences(preferenceKey);
			    		
			    		populateNonInstructionalDays(selectedDateString, durationString, calendarPreferences);
			
			    		m_preferencesManager.setNonInstructionalDayPreferences(preferenceKey, calendarPreferences);
					}
	    		}
	    		else
	    		{
		    		String preferenceKey = userValue.getUserId() + "-" + classroomId;
		    		CalendarPreferences calendarPreferences = m_preferencesManager.getCalendarPreferences(preferenceKey);
		    		
		    		populateNonInstructionalDays(selectedDateString, durationString, calendarPreferences);
		
		    		m_preferencesManager.setNonInstructionalDayPreferences(preferenceKey, calendarPreferences);
	    		}
    		}
    	}
    	catch(Exception e)
    	{
    		logger.error(e.getMessage());
    		return display(request, modelMap, e.getMessage());
    	}

		return display(request, modelMap, null);
    }

	private void populateNonInstructionalDays(String selectedDateString, String durationString, CalendarPreferences calendarPreferences)
	{
		int duration = Integer.parseInt(durationString);
		
		if(duration > 1)
		{
			// Loop through the durations to add selected dates
			for(int a = 0; a < duration; a++)
			{
				LyceaDate selectedDate = CalendarUtility.convertToLyceaDate(selectedDateString);
				selectedDate.add(Calendar.DAY_OF_MONTH, a);
				
				calendarPreferences.addNonInstructionalDay(selectedDate);
			}
		}
		else
		{
			LyceaDate selectedDate = CalendarUtility.convertToLyceaDate(selectedDateString);
			calendarPreferences.addNonInstructionalDay(selectedDate);
		}
	}
    
	//Added for CSI-179--start
	/**
	 * This function will populate the datesToRemove array
	 * with the days that follow the selected date, contained in datesToRemove[0],
	 * the duration being determined by the daysToIncrement variable
	 */
	private void populateNonInstructionalDays(String[] datesToRemove) {
		DateFormat formatter ; 
		Date date = new Date();
		formatter = new SimpleDateFormat("M/d/yyyy");
		String selectedDate = (String)datesToRemove[0];
		Date newDate;
		try {
			date = (Date)formatter.parse(selectedDate); 
			Calendar cal=Calendar.getInstance();
			cal.setTime(date);
			for (int i=1;i<datesToRemove.length;i++){
				cal.add(Calendar.DATE, 1);
				newDate=cal.getTime();
				datesToRemove[i]=new String((String)formatter.format(newDate));
			}
		}catch(ParseException e)
		{
			logger.error(e.getMessage());
		}
	}
	//Added for CSI-179--end
	/**
	 * 
	 * @param classroomManager
	 * 
	 * @author Brendan Clemenzi
	 */
    public void setClassroomManager(ClassroomManager classroomManager) 
    {
    	m_classroomManager = classroomManager;
    }

    /**
     * 
     * @return
     * 
     * @author Brendan Clemenzi
     */
    public ClassroomManager getClassroomManager() 
    {
        return m_classroomManager;
    }
    
	/**
	 * 
	 * @param assignmentManager
	 * 
	 * @author Brendan Clemenzi
	 */
    public void setAssignmentManager(AssignmentManager assignmentManager) 
    {
    	m_assignmentManager = assignmentManager;
    }

    /**
     * 
     * @return
     * 
     * @author Brendan Clemenzi
     */
    public AssignmentManager getAssignmentManager() 
    {
        return m_assignmentManager;
    }
    
	/**
	 * 
	 * @param preferencesManager
	 * 
	 * @author Brendan Clemenzi
	 */
    public void setPreferencesManager(CalendarPreferencesManager preferencesManager) 
    {
    	m_preferencesManager = preferencesManager;
    }

    /**
     * 
     * @return m_preferencesManager
     * 
     * @author Brendan Clemenzi
     */
    public CalendarPreferencesManager getPreferencesManager() 
    {
        return m_preferencesManager;
    }
}
