/**
 * File Name: ScanningController.java
 * Description : This Controller class does the functionality of loading
 *               the file to the file server and the cancellation of file upload process.
 *  @author  : Infosys Technologies Limited
 *  Modification Log
 *  ================
 *  Ver    Date                   Author          		 	      Modification
 *  ---    ------------           ----------------------------     -------------
 *  1.0    Oct 14, 2006           Infosys Technologies Limited     Created
 ***********************************************************************************************
 */
package com.harcourt.hoap.web.scanning;

import hrw.server.HRWServerProperties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lycea.util.LyceaGUID;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import util.StringUtil;
import com.harcourt.activity.ActivityManager;
import com.harcourt.hoap.scanning.service.ScanningImportManager;
import com.harcourt.hoap.scanning.service.timer.BatchTimer;
import com.harcourt.hoap.scanning.utility.ScanningUtility;
import com.harcourt.hoap.util.ApplicationKeys;
import com.harcourt.session.ISessionManager;
import com.harcourt.user.vo.UserValue;

public class ScanningController implements Controller {

	/** Logger for this class and subclasses */
	protected final Log logger = LogFactory.getLog(getClass());

	private static final String MATCH = "MATCH";

	private static final String FORMAT_MISMATCH = "FORMAT_MISMATCH";

	private static final String NAME_MISMATCH = "NAME_MISMATCH";

	private static final String ERROR_PAGE = "/hrw/HRWErrorPage.jsp";

	private static final String THIS_PAGE = "assign-and-import.jsp";

	private String path;

	private int successImport;

	private String assignmentName;

	private ISessionManager m_sessionManager = null;

	private static final String ERROR_MSG = "Error occur while uploading batch file";

	private static final int MIN_LEN_LAST_INDX_OF_RESOURCE_NAME =10;

	/**
	 * This method receives the uploaded file and calls the methods
	 * onImport,onError or onCancel depending upon whether to upload the file,
	 * or to log error or to cancel the process of upload of file.
	 *
	 * @param request,
	 *            response
	 * @return ModelAndView
	 */

	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		MultipartFile multipartFile = null;
		ScanningImportManager manager = new ScanningImportManager();
		MultipartHttpServletRequest multipartRequest = null;
		Map modelMap = new HashMap();
		Map inputMap = new HashMap();
		m_sessionManager = (ISessionManager) request
				.getAttribute(ApplicationKeys.Session.SESSION_MANAGER);
		String errorCode = "";
		String serverPath = "";
		String batchId = "";
		String fileName = "";
		String userId = "";
		String contentId = (String) request.getParameter("contentId");
		String resourceName = (String) request.getParameter("resourceName");
		String action = (String) request.getParameter("actionID");
		ActivityManager activityManager = null;
		String sessionId = "";
		String originalFilename = "";
		UserValue userValue = (UserValue) m_sessionManager
				.getValue(ApplicationKeys.Session.USER_VALUE);
		if (userValue != null) {
			userId = userValue.getUserId();

		}
		String isbn = StringUtil.emptyIfNull((String) m_sessionManager
				.getValue(ApplicationKeys.Session.ISBN));
		String classId = (String) m_sessionManager
				.getValue(ApplicationKeys.Session.CLASSROOM_ID);


		try {
			if (action.equalsIgnoreCase("IMPORT")) {
				activityManager = new ActivityManager();

				String syllabusItemId = activityManager.getSyllabusItemId(true,
					classId, userId, contentId);
				multipartRequest = (MultipartHttpServletRequest) request;
				HttpSession session = multipartRequest.getSession(true);
				sessionId = StringUtil.emptyIfNull(session.getId());

				multipartFile = multipartRequest.getFile("file");

				serverPath = ScanningUtility
						.getConfigAttributeValue("FILE_PATH_EV_XML");
				batchId = LyceaGUID.generateGUID();
				fileName = batchId + ".xml";
				path = serverPath + "/" + fileName;
				originalFilename =multipartFile.getOriginalFilename();

				inputMap.put("batchId", batchId);
				inputMap.put("xmlBatchFileName", originalFilename);
				inputMap.put("userId", userId);
				inputMap.put("contentId", contentId);
				inputMap.put("isbn", isbn);
				inputMap.put("classId", classId);
				inputMap.put("sessionId", sessionId);
				inputMap.put("syllabusItemId", syllabusItemId);
				inputMap.put("resourceName", resourceName);
				// Check for format mismatch and name mismatch errors.
				errorCode = onError(multipartFile, resourceName);

				if (errorCode.equalsIgnoreCase(MATCH)) {

					inputMap.put("assignmentName", assignmentName);
					manager.addToAuditLog(inputMap);
					// Assignment name and file name matches.So save the file in
					// the file server.
					onImport(multipartFile);
					modelMap.put("errorCode", errorCode);
					// Insert meta-data into HRW_XML_BATCH_UPLOAD table.
					successImport = manager.insertOnImport(inputMap);

					if (successImport == 0) {
						manager.updateAuditLog(inputMap);
						manager.insertUploadStatus(inputMap);
					    new BatchTimer(batchId);
					}
					if (successImport != 0) {
						// If insert fails then do as "Cancel" button
						// functionality.
						onCancel();
					}

					response.sendRedirect("assign-and-import.jsp?errorCode="
							+ errorCode);
					return null;
				} else {
					// Either Format Mismatch or Name Mismatch.
					modelMap.put("errorCode", errorCode);
					response
							.sendRedirect("assign_and_import_errors.jsp?errorCode="
									+ errorCode
									+ "&resourceName="
									+ resourceName
									+ "&assignmentNameInXml="
									+ assignmentName);
					return null;
				}

			} else if (action.equalsIgnoreCase("CANCEL")) {
				// Delete the uploaded file from the file server.
				onCancel();
				response
						.sendRedirect("assign-and-import.jsp?errorCode=CANCELIMPORT");
				return null;
			} else if (action.equalsIgnoreCase("DISPLAY")) {
				modelMap.put("userId", userId);
				modelMap.put("contentId", contentId);
				modelMap.put("isbn", isbn);
				modelMap.put("classId", classId);
				modelMap.put("userId", userId);
				modelMap.put("resourceName", resourceName);
			}
		} catch (Exception e) {
			logger.error(e);
			response.sendRedirect(ERROR_PAGE + "?Page=" + THIS_PAGE
					+ "&ErrMsg=" + ERROR_MSG + "&Home="
					+ HRWServerProperties.get_TEACHER_HUB_PAGE());
			return null;
		}

		return new ModelAndView("assign-and-import.jsp", modelMap);

	}

	/**
	 * This method is used to load the file into file server.
	 * @param file
	 * @throws IOException
	 */
	public void onImport(MultipartFile file) throws IOException {

		byte[] inputByte = file.getBytes();

		FileOutputStream fileStream = new FileOutputStream(path);
		fileStream.write(inputByte);
	}

	/**
	 * This is the method for checking Format Error and Name Mismatch error.
	 *
	 * @param file,resourceName
	 * @return errorCode
	 */

	public String onError(MultipartFile file, String resourceName)
			throws Exception {
		String errorCode = MATCH;
		String fileType = file.getContentType();
		ScanningImportManager manager = new ScanningImportManager();

		if (file == null || file.isEmpty()) {
			// Error Code=1 for Wrong File Format.
			errorCode = FORMAT_MISMATCH;
			return errorCode;
		} else if (fileType.indexOf("xml") == -1) {
			errorCode = FORMAT_MISMATCH;
			return errorCode;
		}

		// Getting Assignment Name using SAX parsing.
		assignmentName = manager.getAssignmentName(file);
		String  modifiedassignmentName = assignmentName.replace(' ','_');
		assignmentName = modifiedassignmentName ;
		if (!modifiedassignmentName.equals(resourceName)) {
			errorCode = checkresourceName(resourceName,modifiedassignmentName);
		}
		return errorCode;
	}

	/**
	 * This metod is used to delete the file from the file server.
	 *
	 */
	public void onCancel() {

		File file = new File(path);

		if (file.exists()) {
			file.delete();
		}
}
	/**
	 * This is the method for checking Name Mismatch error.
	 * @param modifiedassignmentName ,modifiedassignmentName
	 * @return errorCode
	 */
	private String checkresourceName(String resourceName,
			String modifiedassignmentName) {
		String errorCode = MATCH;
		if (resourceName.length() > modifiedassignmentName.length()) {
			String lastIndexOfresourceName = resourceName.substring(
					modifiedassignmentName.length() + 1, resourceName.length());
			if (lastIndexOfresourceName.length() >= MIN_LEN_LAST_INDX_OF_RESOURCE_NAME) {
				try {
					new Long(lastIndexOfresourceName).longValue();
				} catch (NumberFormatException e) {
					errorCode = NAME_MISMATCH;
				}
			} else {
				errorCode = NAME_MISMATCH;
			}

		}else  {
			errorCode = NAME_MISMATCH;
		}
		return errorCode;
	}

}

