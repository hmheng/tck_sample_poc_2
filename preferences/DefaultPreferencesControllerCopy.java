package com.harcourt.hoap.web.preferences;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import util.StringUtil;

import com.harcourt.hoap.util.ApplicationKeys;
import com.harcourt.preferences.TestPreferencesManager;
import com.harcourt.preferences.vo.TestPreferences;
import com.harcourt.session.ISessionManager;
import com.harcourt.user.vo.UserValue;
import com.harcourt.util.http.RequestUtil;

/**
 * The DefaultPreferencesController will be responsible for executing the view/create/edit 
 * fuctions required in having gerneral preferences.
 * 
 * @author bclemenzi
 */
public class DefaultPreferencesControllerCopy implements Controller 
{
	

    /** Logger for this class and subclasses */
    protected final Log m_logger = LogFactory.getLog(getClass());
    
    /** */
    private ISessionManager m_sessionManager;
    
    /** */
    private TestPreferencesManager m_preferencesManager;

    /**
     * The handleRequest method will generate the welcome view for the end user.  The display 
     * will be rendered by utilizing a jsp utilized by Spring's ModelAndView object.
     * 
     * @param HttpServletRequest
     * @param HttpServletResponse
     */
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	ModelAndView modelView;
		Map modelMap = new HashMap();
		
		// Get a handle to the the user session object, then retrieve the user value object.
		m_sessionManager = (ISessionManager)request.getAttribute(ApplicationKeys.Session.SESSION_MANAGER);
		
        // Invoke the action method requested.
        String action = "display";
		if (request.getParameter(ApplicationKeys.HttpRequest.ACTION) != null)
		{
		    action = request.getParameter(ApplicationKeys.HttpRequest.ACTION);
		}
		
		// Determine what action to invoke.
		if(action.equalsIgnoreCase("process"))
		{
		    modelView = process(request, modelMap);
		}
		else
		{
		    modelView = display(request, modelMap, null);
		}
        
        return modelView;
    }
    
    /**
     * The display method will be responsible for rendering the preferences form.  This method will also be executed 
     * as the success/error condition of the create/edit methods of this class.  If the end-user should be notified 
     * with an alert message, the developer should populate the 'systemMessage' parameter passed in.
     * 
     * @param request
     * @param modelMap
     * @param errorMessage
     * @return
     */
    private ModelAndView display(HttpServletRequest request, Map modelMap, String systemMessage)
    {
    	UserValue userValue = (UserValue)m_sessionManager.getValue(ApplicationKeys.Session.USER_VALUE);
    	
        if(!StringUtil.isNullOrEmpty(systemMessage))
        {
            modelMap.put("systemMessage", systemMessage);
        }

		modelMap.put("userValue", userValue);
		
		try
    	{
			TestPreferences testPreferences = m_preferencesManager.getTestPreferences(userValue.getUserId());
			m_sessionManager.putValue(ApplicationKeys.Session.PREFERENCES, testPreferences);
			modelMap.put(TestPreferences.Tag.ATTEMPTS, testPreferences.getAttempts());
			modelMap.put(TestPreferences.Tag.RANDOMIZE_ORDER, testPreferences.getRandomizeOrder());
			modelMap.put(TestPreferences.Tag.ALLOW_STUDENT_HOW_MANY_SUBMITS, testPreferences.getAllowStudentHowManySubmits());
			modelMap.put(TestPreferences.Tag.KEEP_WHICH_SCORE, testPreferences.getKeepWhichScore());
			modelMap.put(TestPreferences.Tag.ALLOW_STUDENT_TO_SEE_TEST, testPreferences.getAllowStudentToSeeTest());
			modelMap.put(TestPreferences.Tag.ALLOW_STUDENT_TO_SEE_TEST_SCORE, testPreferences.getAllowStudentToSeeTestScore());
			modelMap.put(TestPreferences.Tag.ALLOW_STUDENT_TO_SEE_CORRECT_ANSWER, testPreferences.getAllowStudentToViewCorrectAnswer());
			modelMap.put(TestPreferences.Tag.ALLOW_STUDENT_TO_SEE_POINT_VALUE, testPreferences.getAllowStudentToSeePointValue());
			modelMap.put(TestPreferences.Tag.TEST_FORMAT, testPreferences.getTestFormat());
			modelMap.put(TestPreferences.Tag.SHOW_BUBBLESHEET_GRIDS, testPreferences.getShowBubbleSheetGrids());
    	}
    	catch(Exception e)
    	{
    		m_logger.error(e.getMessage());
    		modelMap.put("systemMessage", e.getMessage());
    	}
		
		
		return new ModelAndView("preferences/default-form.jsp", modelMap);
    }

	/**
     * The process() method executes the create functions required to add 
     * default preferences to the user account in question.  Once complete, the user 
     * will be redirected to the display method to alert the use of the 
     * success/error message.
     * 
     * @param request
     * @param modelMap
     * @return ModelAndView
     */
    private ModelAndView process(HttpServletRequest request, Map modelMap)
    {
    	try
    	{
    	
			//Init 
			RequestUtil ru = new RequestUtil(request, RequestUtil.RequestOptions.PARAMETER);
			TestPreferences testPreferences = (TestPreferences)m_sessionManager.getValue(ApplicationKeys.Session.PREFERENCES);
			
			//Set
			testPreferences.setAttempts(ru.get(TestPreferences.Tag.ATTEMPTS));
			testPreferences.setRandomizeOrder(ru.get(TestPreferences.Tag.RANDOMIZE_ORDER));
			testPreferences.setKeepWhichScore(ru.get(TestPreferences.Tag.KEEP_WHICH_SCORE));
			testPreferences.setAllowStudentHowManySubmits(ru.get(TestPreferences.Tag.ALLOW_STUDENT_HOW_MANY_SUBMITS));
			testPreferences.setKeepWhichScore(ru.get(TestPreferences.Tag.KEEP_WHICH_SCORE));
			testPreferences.setAllowStudentToSeeTest(ru.get(TestPreferences.Tag.ALLOW_STUDENT_TO_SEE_TEST));
			testPreferences.setAllowStudentToSeeTestScore(ru.get(TestPreferences.Tag.ALLOW_STUDENT_TO_SEE_TEST_SCORE));
			testPreferences.setAllowStudentToViewCorrectAnswer(ru.get(TestPreferences.Tag.ALLOW_STUDENT_TO_SEE_CORRECT_ANSWER));
			testPreferences.setAllowStudentToSeePointValue(ru.get(TestPreferences.Tag.ALLOW_STUDENT_TO_SEE_POINT_VALUE));
			testPreferences.setTestFormat(ru.get(TestPreferences.Tag.TEST_FORMAT));
			testPreferences.setShowBubbleSheetGrids(ru.get(TestPreferences.Tag.SHOW_BUBBLESHEET_GRIDS));
		
			//Commit
    		m_preferencesManager.setTestPreferences(testPreferences);
    		
    	}
    	catch(Exception e)
    	{
    		m_logger.error(e.getMessage());
    		return display(request, modelMap, e.getMessage());
    	}

    	String systemMessage = "You have saved your Default Test Settings Preferences. This will apply only to future assignments.";
		return display(request, modelMap, systemMessage);
    }
    
	/**
	 * 
	 * @param preferencesManager
	 * 
	 * @author Brendan Clemenzi
	 */
    public void setPreferencesManager(TestPreferencesManager preferencesManager) 
    {
    	m_preferencesManager = preferencesManager;
    }

    /**
     * 
     * @return m_preferencesManager
     * 
     * @author Brendan Clemenzi
     */
    public TestPreferencesManager getPreferencesManager() 
    {
        return m_preferencesManager;
    }
}
