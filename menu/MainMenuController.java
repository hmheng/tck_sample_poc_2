package com.harcourt.hoap.web.menu;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import util.StringUtil;

import com.harcourt.hoap.util.ApplicationKeys;
import com.harcourt.session.ISessionManager;
import com.harcourt.user.vo.UserValue;

/**
 * The MainMenuController will be responsible for rendering the main menu frame of the HOAP application.
 * 
 * @author bclemenzi
 */
public class MainMenuController implements Controller 
{
    /** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    /**
     * The handleRequest method will generate the menu view for the end user.  The display 
     * will be rendered by utilizing a jsp utilized by Spring's ModelAndView object.
     * 
     * @param HttpServletRequest
     * @param HttpServletResponse
     */
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	Map modelMap = new HashMap();
    	logger.debug("MainMenuController - returning main menu view");

		// Get a handle to the the user session object, then retrieve the user value object.
		ISessionManager sessionManager = (ISessionManager)request.getAttribute(ApplicationKeys.Session.SESSION_MANAGER);
		UserValue userValue = (UserValue)sessionManager.getValue(ApplicationKeys.Session.USER_VALUE);
		String entrySource = (String)sessionManager.getValue("entrySource");
		String pageValue = (String)sessionManager.getValue("pageValue"); //FLIT-1118 Page value to hit Reports page directly from Dashboard
		sessionManager.removeValue("pageValue");//FLIT-1118
		// put current date into string for use by page view
		String isEplanner = StringUtil.emptyIfNull(sessionManager.getValue("isEplanner")); 
		sessionManager.removeValue("isEplanner");
		SimpleDateFormat formatter = new SimpleDateFormat ("MMMM d, yyyy");
		Date currDate = new Date();
		String currentDateString = formatter.format(currDate);		 		 
		
		logger.debug("MainMenuController - User: " + userValue.getUsername());
		
		//Changes to test code commit in branch 2
		logger.debug("MainMenuController - User: " + userValue.getUsername());
		
		// Add the user object to the model map, for use within the jsp.
		modelMap.put("pageValue", pageValue);//FLIT-1118
		modelMap.put("userValue", userValue);
		modelMap.put("currentDateString", currentDateString);
		modelMap.put("entrySource", entrySource);
		modelMap.put("isEplanner", isEplanner);
		
		return new ModelAndView("menu/main-menu.jsp", modelMap);
    }
}
