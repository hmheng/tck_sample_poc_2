package com.harcourt.hoap.web.menu;

import hrw.error.HRWException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import util.StringUtil;

import com.harcourt.activity.util.ResourceTypeUtility.ICategoryType;
import com.harcourt.eproduct.vo.SecureProgramSubscriptionsValue;
import com.harcourt.hoap.util.ApplicationKeys;
import com.harcourt.organization.classroom.ClassroomManager;
import com.harcourt.organization.classroom.ClassroomProductManager;
import com.harcourt.organization.classroom.exception.ClassroomCheckedException;
import com.harcourt.preferences.InterventionPreferencesManager;
import com.harcourt.preferences.exception.PreferenceCheckedException;
import com.harcourt.preferences.vo.InterventionPreferences;
import com.harcourt.session.ISessionManager;
import com.harcourt.user.vo.UserValue;

import ecom.sprog.data.SProg;

/**
 * The MainMenuController will be responsible for rendering the content menu frame of the HOAP 
 * application.  This display will contain the classroom and book selections used to filter the 
 * activity lists within the display area of the application.
 * 
 * @author bclemenzi
 */
public class AssignmentMenuController implements Controller 
{
    /** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());
    
    /** */
    private ClassroomManager m_classroomManager;
    
    /** */
    private ISessionManager m_sessionManager;

    /**
     * The handleRequest method will generate the menu view for the end user.  The display 
     * will be rendered by utilizing a jsp utilized by Spring's ModelAndView object.
     * 
     * @param HttpServletRequest
     * @param HttpServletResponse
     */
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	ModelAndView modelView;
		Map modelMap = new HashMap();
		
		// Get a handle to the the user session object, then retrieve the user value object.
		m_sessionManager = (ISessionManager)request.getAttribute(ApplicationKeys.Session.SESSION_MANAGER);
		
        // Invoke the action method requested.
        String action = "display";
		if (request.getParameter(ApplicationKeys.HttpRequest.ACTION) != null)
		{
		    action = request.getParameter(ApplicationKeys.HttpRequest.ACTION);
		}
		
		if(action.equalsIgnoreCase("process"))
		{
		    modelView = process(request, modelMap);
		}
		else
		{
		    modelView = display(request, modelMap, null);
		}
        
        return modelView;
    }
    
    /**
     * The display method is responsible for rendering the empty centent menu.
     * 
     * @param request
     * @param modelMap
     * @param errorMessage
     * @return
     */
    private ModelAndView display(HttpServletRequest request, Map modelMap, String errorMessage)
    {
		UserValue userValue = (UserValue)m_sessionManager.getValue(ApplicationKeys.Session.USER_VALUE);
    	String classroomId = (String)m_sessionManager.getValue(ApplicationKeys.Session.CLASSROOM_ID);
    	String isbn = (String)m_sessionManager.getValue(ApplicationKeys.Session.ISBN);
    	//Added for Eplanner--Check if the user has the viewBy in the request
    	
    	String viewBy = (String)request.getParameter("viewBy");
    	if(StringUtil.isNullOrEmpty(viewBy)){ //Added for flit-689
    		//If it is not available in session check if it is there in the request
    		viewBy=(String)m_sessionManager.getValue("viewBy");
    	}
    	String activeAsmts = (String)m_sessionManager.getValue("activeAsmts");
    	String activityType = (String)m_sessionManager.getValue(ApplicationKeys.Session.ACTIVITY_TYPE);
    	String mwsCreateAssign = StringUtil.emptyIfNull(request.getParameter("mwsCreateAssign"));
        	
        if(!StringUtil.isNullOrEmpty(errorMessage))
        {
            modelMap.put("errorMessage", errorMessage);
        }
    	
    	try
		{
    		Collection classrooms = m_classroomManager.getActiveClassroomsByUserId(userValue.getUserId());
    		
    		if(!StringUtil.isNullOrEmpty(classroomId))
    		{
    			ClassroomProductManager classroomProductManager = ClassroomProductManager.getClassroomProducts(classroomId);
    			Collection secureProgram = classroomProductManager.getSecureProgramSubscriptions();
    			
    			modelMap.put("books", secureProgram);
    		}
			
			// Add the required objects to the model map, for use within the jsp.
			modelMap.put("classrooms", classrooms);
			modelMap.put("userValue", userValue);
			modelMap.put("classroomId", classroomId);
			modelMap.put("isbn", isbn);
			modelMap.put("viewBy", viewBy);
			modelMap.put("activeAsmts", activeAsmts);
			modelMap.put("activityType", activityType);
			modelMap.put("mwsCreateAssign", mwsCreateAssign);
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
		}
		
    	return new ModelAndView("menu/assignment-menu.jsp", modelMap);
    }
    
    /**
     * The process method is responsible for rendering the centent menu with classroom and book data.
     * 
     * @param request
     * @param modelMap
     * @return
     */
    private ModelAndView process(HttpServletRequest request, Map modelMap)
    {
    	UserValue userValue = (UserValue)m_sessionManager.getValue("UserValue");
    	String classroomId = StringUtil.emptyIfNull(request.getParameter("classroomId"));
    	String isbn = StringUtil.emptyIfNull(request.getParameter("isbn"));
    	String viewBy = StringUtil.emptyIfNull(request.getParameter("viewBy"));    	
    	String grade = StringUtil.emptyIfNull(request.getParameter("grade"));
    	String activityType = StringUtil.emptyIfNull(request.getParameter("activityType"));
    	String activeAsmts = StringUtil.emptyIfNull(request.getParameter("activeAsmts"));
    	String from = StringUtil.emptyIfNull(request.getParameter("from"));
    	String selectedBookName = StringUtil.emptyIfNull(request.getParameter("selectedBookName"));
    	
    	// Add the request parameters to session
    	m_sessionManager.putValue(ApplicationKeys.Session.CLASSROOM_ID, classroomId);
    	m_sessionManager.putValue(ApplicationKeys.Session.ISBN, isbn);
    	m_sessionManager.putValue("viewBy", viewBy);
    	m_sessionManager.putValue("activeAsmts", activeAsmts);
    	m_sessionManager.putValue(ApplicationKeys.Session.GRADE, grade);
    	m_sessionManager.putValue(ApplicationKeys.Session.ACTIVITY_TYPE, activityType);
    	m_sessionManager.putValue(ApplicationKeys.Session.SELECTEDBOOKNAME, selectedBookName);
    	try
		{
    		Collection classrooms = m_classroomManager.getActiveClassroomsByUserId(userValue.getUserId());

    		ClassroomProductManager classroomProductManager = ClassroomProductManager.getClassroomProducts(classroomId);
			Collection securePrograms = classroomProductManager.getSecureProgramSubscriptions();
			modelMap.put("books", securePrograms);
			
    		Collection activeBooks = new ArrayList();
			// Loop through the books to determine if there is more than one active book.
			for (Iterator i = securePrograms.iterator(); i.hasNext();)
			{	
				SecureProgramSubscriptionsValue secureProgramSubscription = (SecureProgramSubscriptionsValue)i.next();
				SProg secureProgram = secureProgramSubscription.getSecureProgram();
				
				activeBooks.add(secureProgram);
            }

    		// CC 7-20: if we submitted by changing the class dropdown, we also need to make sure we either
    		// clear out the ISBN or select the correct one if the new class only has one active book.
    		if("class".equals(from))
    		{
        		if(activeBooks.size() == 1)
        		{
        			isbn = ((SProg)(activeBooks.iterator().next())).getStudentEditionISBN();
        		}
        		else
        		{
        			isbn = "";
        		}
            	m_sessionManager.putValue(ApplicationKeys.Session.ISBN, isbn);
    		}
    		
			// Add the intervention preferences object.
			InterventionPreferencesManager interventionPreferencesManager = new InterventionPreferencesManager();
			InterventionPreferences interventionPreferences = interventionPreferencesManager.getInterventionPreferences(userValue.getUserId()+ "-" + classroomId + "-" + isbn);
			modelMap.put("interventionPreferences", interventionPreferences);
			
			// Check if we are displaying offline activity and default view to by 'res'
			if (activityType.equals(ICategoryType.OFFLINE_ACTIVITY))
			{
				viewBy = "res";
			}
			
			// Add the required objects to the model map, for use within the jsp.
    		modelMap.put("classroomId", classroomId);
    		modelMap.put("isbn", isbn);
    		modelMap.put("viewBy", viewBy);
    		modelMap.put("grade", grade);
    		modelMap.put("activityType", activityType);
			modelMap.put("classrooms", classrooms);
			modelMap.put("userValue", userValue);
			modelMap.put("activeAsmts", activeAsmts);
		}
		catch (ClassroomCheckedException e)
		{
            logger.error(e.getMessage());
            return display(request, modelMap, "Unable to determine classroom and book list.");
		}
		catch (HRWException e)
		{
            logger.error(e.getMessage());
            return display(request, modelMap, "Unable to determine classroom and book list.");
		}
		catch (PreferenceCheckedException e)
		{
            logger.error(e.getMessage());
            return display(request, modelMap, "Unable to determine intervention preferences.");
		}

		return new ModelAndView("menu/assignment-menu.jsp", modelMap);
    }
    
	/**
	 * 
	 * @param classroomManager
	 * 
	 * @author Brendan Clemenzi
	 */
    public void setClassroomManager(ClassroomManager classroomManager) 
    {
    	m_classroomManager = classroomManager;
    }

    /**
     * 
     * @return
     * 
     * @author Brendan Clemenzi
     */
    public ClassroomManager getClassroomManager() 
    {
        return m_classroomManager;
    }
}
