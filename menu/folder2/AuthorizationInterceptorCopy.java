package com.harcourt.hoap.web.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lycea.core.session.LyceaUserSessionUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import util.StringUtil;

import com.harcourt.hoap.util.ApplicationKeys;
import com.harcourt.session.ISessionManager;
import com.harcourt.session.SessionFactory;
import com.harcourt.session.exception.SessionCheckedException;
import com.harcourt.user.vo.UserValue;
import com.hrw.sso.util.SSOHelper;

/**
 * The AuthorizationInterceptor primary purpose is to initialize the session management 
 * system.  This will preload session data for the developer that is normally called 
 * on each page.  The interceptor is also responsible for authenticating the user
 * access the page in question.  If the user is accessing the page in error, they will 
 * be forwarded to an error notification page.
 * 
 * This interceptor will store the following values as HTTP Request attributes.
 *  1. userValue - The user value object for the current user account accessing the site. 
 *  2. sessionManager - The current session object for the user accessing the site.
 * 
 * @author bclemenzi
 */
public class AuthorizationInterceptorCopy extends HandlerInterceptorAdapter 
{
    /** Logger for this class and subclasses */
    protected final Log m_logger = LogFactory.getLog(getClass());
    
    /**
     *  The preHandle method of this class is used to aquire the user session object from Lycea along with
     *  storing the session and user value objects in the HTTP Request header.
     *  
     *  @param request
     *  @param response
     *  @param handler
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
    	final String debugString = getClass().getName() + ".preHandle(HttpServletRequest, HttpServletResponse, Object)";
		boolean isSSOCookieAvailable = false;
		String ssoRequestURL = null;
    	try
		{
    		// Added for SSO Implementation FWK-563 *** Start ***
    		String ssoCookieValue = SSOHelper.getSSOCookie(request, response);
    		
    		if(!StringUtil.isNullOrEmpty(ssoCookieValue)) {
    			isSSOCookieAvailable = true;
    			ssoRequestURL = SSOHelper.getSSORequestURL(ssoCookieValue, request, response);
    		}
    		// Added for SSO Implementation FWK-563 *** End ***
    		
    		ISessionManager sessionManager = SessionFactory.getSessionManager();
    		sessionManager.initialize(request, response); 
   
			String sessionId = LyceaUserSessionUtil.getSessionId(request);
			
			// If session ID is null, session has not been created yet so user must not have logged in yet either.
			// Send them to the login page.
			if (sessionId == null)
			{
			    throw new SessionCheckedException(SessionCheckedException.Codes.MISSING_SESSION_ID, debugString);
			}

			// Check if the session timed out.
			if (!LyceaUserSessionUtil.checkSession(request, response))
			{   
			    throw new SessionCheckedException(SessionCheckedException.Codes.SESSION_TIMEOUT, sessionId, debugString);
			}           

			// Check the code commit changes on new branches
			
			if (!LyceaUserSessionUtil.checkSession(request, response))
			{   
			    throw new SessionCheckedException(SessionCheckedException.Codes.SESSION_TIMEOUT, sessionId, debugString);
			}           

			
			sessionManager.setSessionID(sessionId);
			
			try
			{
				sessionManager.load();
			}
			catch (IOException e)
			{
				throw new SessionCheckedException(SessionCheckedException.Codes.ERROR_LOADING_SESSION, sessionId, debugString);
			}
			
			UserValue userValue = (UserValue)sessionManager.getValue(ApplicationKeys.Session.USER_VALUE);

			// If there's no user ID in the session, user must not have logged in yet. Send them to the login page.
			if (userValue == null)
			{
			    throw new SessionCheckedException(SessionCheckedException.Codes.MISSING_USER_VALUE, debugString);
			}
			else
			{
				if(userValue.getUserId().equals(""))
				{
					throw new SessionCheckedException(SessionCheckedException.Codes.MISSING_USER_VALUE, debugString);
				}
			}
			
			// Added for SSO Implementation FWK-563 *** Start ***
			// Check for deep link session if so, destroy that and start SP initiated SSO session  
			if(isSSOCookieAvailable && SSOHelper.isDeeplinkSession(sessionManager)) {
				System.out.println("[Hoap] Destroying deep link session and starting sp initiated SSO session");
				SSOHelper.destroySession(request, response);
				request.getRequestDispatcher(ssoRequestURL).forward(request, response);
			}
			// Added for SSO Implementation FWK-563 *** End ***		

			// Store the session and user value obejcts in the http request header to be 
			// used later by the controller be invoked.
			request.setAttribute(ApplicationKeys.Session.SESSION_MANAGER, sessionManager);
		}
		catch (SessionCheckedException e)
		{
			m_logger.error(e.getMessage());
			if(e.getCode() == SessionCheckedException.Codes.SESSION_TIMEOUT)
			{
				// gracefully send the user back to the login page if the session
				// has timed out
				// Added for SSO Implementation FWK-563 *** Start ***
				if(isSSOCookieAvailable) {
					// session is not available, so initiate SP SSO session
					request.getRequestDispatcher(ssoRequestURL).forward(request, response);
				}
				// Added for SSO Implementation FWK-563 *** End ***	
				
				request.getRequestDispatcher("/error/session_timeout.jsp").forward(request, response);
			}
            // Modified for FWK-991. By passing 403.htm by using 403.jsp  
			request.getRequestDispatcher("/error/403.jsp").forward(request, response);
			return false;
		}
    	
        return true;
    }
    
    /**
     * The postHandle method of this class is used to save the session state of the user.
     */
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {
    	try
		{
    		ISessionManager sessionManager = (ISessionManager)request.getAttribute(ApplicationKeys.Session.SESSION_MANAGER);
    		
			// If we have a session object, save it's contents.
			if(sessionManager != null)
			{
				sessionManager.save();
			}
		}
		catch (IOException e)
		{
			// If and exception occurs while saving, log and error but don't notify the end-user.
			m_logger.warn(e.getMessage());
		}
    }
}
